Another example of using `https://bitbucket.org/ada/python-ciur` by parsing site exchange money rates world wide.

Its include:
   * Romania (RO) National Bank + 17 local
   * Moldova (MD) National Bank + 11 local
   * Armenia (AM) National Bank + 20 local
   * Ukraine (UA) National Bank + 2 local

Installment:

.. code-block:: bash

    /your/env/bin/pip3.6 install -e git+ssh://git@github.com/ada/python-ciur#egg=ciur

TODO:
   * DRAW MAP coverage of available country and banks in that repo