"""
>>> test_bank()  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
    "root": {
        "items_list": [
            {
                "currency": "USD",
                "buy": ...,
                "sell": ...
            },
            {
                "currency": "EUR",
                "buy": ...,
                "sell": ...
            },
            {
                "currency": "RUB",
                "buy": ...,
                "sell": ...
            },
            {
                "currency": "GEL",
                "buy": ...,
                "sell": ...
            }
        ]
    }
}
"""
from ciur.shortcuts import pretty_parse, HTTP_HEADERS


def test_bank():
    # TODO 1 - refactor related to req_callback
    # TODO 2 - add this in list of example post requests
    import requests
    url = "http://www.procreditbank.am/all_exchange_frame/table_ajax.php"

    http_headers = {
        "referer": "http://www.procreditbank.am"
    }

    http_headers.update(HTTP_HEADERS)

    def req_callback():
        return requests.Session().post(url, data={
            "date": "2015-11-27",
            "activetab": "1",
            "lang_id": "2"
        }, headers=http_headers)

    print(pretty_parse("exchange_cash.ciur", url, req_callback=req_callback, headers=http_headers))


if __name__ == "__main__":
    test_bank()

