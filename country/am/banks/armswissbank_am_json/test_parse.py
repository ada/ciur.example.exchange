"""
>>> test_bank()  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
        "root": {
            "item_list": [
                {
                    "currency": "USD",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "EUR",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "GBP",
                    "buy": ...,
                    "sell": ...
                },
                ...
            ]
        }
    }
"""
from ciur.shortcuts import pretty_parse_from_resources


def test_bank():
    print(pretty_parse_from_resources("exchange_cash.ciur", "http://www.armswissbank.am/en/"))


if __name__ == "__main__":
    test_bank()
