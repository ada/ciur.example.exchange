"""
>>> test_bank()  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
        "root": {
            "date": "datetime.datetime(..., ..., ..., ..., ...)",
            "item_list": [
                {
                    "currency": "USD",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "GBP",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "CHF",
                    "buy": ...,
                    "sell": ...
                },
                ...
            ]
        }
    }
"""
from ciur.shortcuts import pretty_parse
from ciur.shortcuts import HTTP_HEADERS


def test_bank():
    url = "http://www.acba.am/en"

    def req_callback():
        import requests

        req_session = requests.Session()

        return req_session.get(url, headers=HTTP_HEADERS, verify=False)

    print(pretty_parse("exchange_cash.ciur", url, req_callback=req_callback))


if __name__ == "__main__":
    test_bank()


