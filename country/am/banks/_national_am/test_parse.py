import ciur
from ciur.shortcuts import pretty_parse_from_resources


def test_bank():
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print(pretty_parse_from_resources(
                f,
                "https://www.cba.am/EN/SitePages/ExchangeArchive.aspx"
        ))


if __name__ == "__main__":
    test_bank()

