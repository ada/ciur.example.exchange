from ciur.shortcuts import pretty_parse_from_url
import ciur


def test_bank():
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                "http://credit-agricole.ro/en/exchange-rate"
        )


if __name__ == "__main__":
    test_bank()
