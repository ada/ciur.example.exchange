"""
>>> test_bank()  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
        "root": {
            "date": "datetime.datetime(..., ..., ..., ..., ...)",
            "item_list": [
                {
                    "currency": "EUR",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "USD",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "GBP",
                    "buy": ...,
                    "sell": ...
                },
                ...
            ]
        }
    }
"""
from ciur.shortcuts import pretty_parse_from_url
import ciur


def test_bank():
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                "http://www.librabank.ro/Curs_valutar/"
        )


if __name__ == "__main__":
    test_bank()
