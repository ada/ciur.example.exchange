"""
>> test_bank() # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
    "institute": 785,
    "comparisonDate": "...",
    "fx": [
        {
            "currency": "JPY",
            "precision": 0,
            "exchangeRate": {
                "sell": ...,
                "lastModified": "...",
                "buy": ...
            },
            "nameI18N": "Japanese Yen"
        },
...

"""
from ciur import pretty_json


def test_bank():
    import requests
    res = requests.get("https://www.bcr.ro/bin/erstegroup/fx?institute=785")
    print pretty_json(res.json())


if __name__ == "__main__":
    test_bank()
