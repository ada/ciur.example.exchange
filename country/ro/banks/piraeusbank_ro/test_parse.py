"""
>> test_bank("exchange_cash.ciur")  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
    "root": {
        "date": "datetime.datetime(..., ..., ..., ..., ...)",
        "item_dict": {
            "EUR": {
                "buy": ...,
                "sell": ...
            },
            "USD": {
                "buy": ...,
                "sell": ...
            },
            "CHF": {
                "buy": ...,
                "sell": ...
            },
            "GBP": {
                "buy": ...,
                "sell": ...
            },
            "HUF": {
                "buy": ...,
                "sell": ...
            }
        }
    }
}
"""
from ciur.shortcuts import pretty_parse_from_url
import ciur
import pytest


PARAMS = [
    "exchange_cash.ciur",
    "exchange_cash_dict_example.ciur"
]


@pytest.mark.parametrize("ciur_file", PARAMS)
def test_bank(ciur_file):
    with ciur.open_file(ciur_file, __file__) as f:
        print pretty_parse_from_url(
                f,
                "http://www.piraeusbank.ro/Bank/Tools/Past_exchange_rate.html"
        )


if __name__ == "__main__":
    test_bank(PARAMS[0])
    test_bank(PARAMS[1])
