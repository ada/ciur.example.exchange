import datetime
from ciur.shortcuts import pretty_parse_from_url
import ciur


def test_bank():

    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                "http://www.bnr.ro/files/xml/curs_2015_11_17.xml",
                # or function
                # "http://www.bnr.ro/files/xml/curs_{dt.year}_{dt.month}
                # _{dt.day}.xml".format(dt=datetime.datetime.now()),
                namespace={"c": "http://www.bnr.ro/xsd"}
        )


if __name__ == "__main__":
    test_bank()

