from ciur.shortcuts import pretty_parse_from_url
import ciur


def test_bank():
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                "https://www.bancpost.ro/Bancpost-En/Financial/"
                "Exchange-rate-values"
        )


if __name__ == "__main__":
    test_bank()
