from ciur.shortcuts import pretty_parse_from_url
import ciur

log = ciur.get_logger(__name__)


def test_bank():
    log.info("ceva")
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                "https://www.banca-romaneasca.ro/en/tools-and-resources/"
                "exchange-rates/"
        )


if __name__ == "__main__":
    test_bank()

# import requests
# from lxml.html import html5parser
# data = requests.get("https://www.banca-romaneasca.ro/en/tools-and-resources/exchange-rates/").content
# context = html5parser.document_fromstring(data)  # work
#
# # import lxml.html
# #
# # data = requests.get("https://www.banca-romaneasca.ro/en/tools-and-resources/"exchange-rates/").content
# # tree = lxml.html.fromstring(data)
# #
# # print(tree.xpath("//title/text()"))
# # ['Tools and resources - Banca Romaneasca']
#
# from lxml.html import html5parser, fromstring
# context = fromstring(document.content) # work