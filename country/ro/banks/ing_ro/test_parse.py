"""
>>> test_bank()  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS
{
        "root": {
            "date": "datetime.datetime(..., ..., ..., ..., ...)",
            "item_list": [
                {
                    "currency": "AUD",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "CHF",
                    "buy": ...,
                    "sell": ...
                },
                {
                    "currency": "DKK",
                    "buy": ...,
                    "sell": ...
                },
                ...
            ]
        }
    }
"""
from ciur.shortcuts import pretty_parse_from_url
import ciur


def test_bank():
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                "https://www.ing.ro/ing/curs-valutar.html"
        )


if __name__ == "__main__":
    test_bank()
