from ciur.shortcuts import pretty_parse_from_url
import ciur
import pytest


PARAMS = [
    "http://www.eximb.com/rus/personal/everyday/currency_exchange/",
    "http://www.eximb.com/ukr/personal/everyday/currency_exchange/"
]


@pytest.mark.parametrize("url", PARAMS)
def test_bank(url):
    with ciur.open_file("exchange_cash.ciur", __file__) as f:
        print pretty_parse_from_url(
                f,
                url
        )


if __name__ == "__main__":
    test_bank(PARAMS[0])
    test_bank(PARAMS[1])
