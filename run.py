import sys
import logging
import datetime
import os
import yaml
import glob

from ciur.shortcuts import pretty_parse_from_resources
import ciur.exceptions

LOG = logging.getLogger(__name__)


def parse(ciur_file_path):
    urls_file_path = os.path.join(
        os.path.dirname(ciur_file_path),
        "urls.yml")

    if os.path.exists(urls_file_path):
        with open(urls_file_path) as file_cursor:
            url_config = [
                {
                    "doctype": "*/html",
                    "urls": [i]
                } if not isinstance(i, dict) else i
                for i in yaml.load(file_cursor)
            ]

        with open(ciur_file_path) as f:
            url = url_config[0]["urls"][0].format(today=datetime.datetime.utcnow())

            res = (pretty_parse_from_resources(
                f,
                url,
                doctype=url_config[0]["doctype"]
            ))
            LOG.info(res)
            # return res


def cli(*argv):
    """
    :param argv: command line arguments
    """

    if not argv:
        LOG.error("bank query require, \n"
                  "f.e. country/md/banks/*/exchange_cash.ciur\n"
                  "use posix glob")
        exit(1)

    command = argv[0]

    for path in glob.glob(command):
        print("parse file %s" %(path, ))
        try:
            print (parse(path))
        except ciur.exceptions.CiurBaseException as exc:
            raise
            #LOG.error(exc)


def main():
    cli(*sys.argv[1:])


if __name__ == "__main__":
    main()
